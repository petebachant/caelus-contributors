/*---------------------------------------------------------------------------*
Caelus 5.10
Web:   www.caelus-cml.com
\*---------------------------------------------------------------------------*/
FoamFile
{
    version         2.0;
    format          ascii;
    class           dictionary;
    object          fvSolution;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    p
    {
        solver          GAMG;
        tolerance       1e-06;
        relTol          0.0001;
        smoother        DILUGaussSeidel;
        nPreSweeps      0;
        nPostSweeps     2;
        nFinestSweeps   2;
        cacheAgglomeration true;
        nCellsInCoarsestLevel 20;
        agglomerator    faceAreaPair;
        mergeLevels     1;
        minIter         1;
    }

    pFinal
    {
        solver          GAMG;
        tolerance       1e-08;
        relTol          1e-06;
        smoother        DILUGaussSeidel;
        nPreSweeps      0;
        nPostSweeps     2;
        nFinestSweeps   2;
        cacheAgglomeration true;
        nCellsInCoarsestLevel 20;
        agglomerator    faceAreaPair;
        mergeLevels     1;
        minIter         1;
    }

    "(rho|U|h|k|epsilon|omega)"
    {
        solver          PBiCG;
        preconditioner
        {
            preconditioner  DILU;
        }
        tolerance       1e-08;
        relTol          1e-05;
    }
    "(rho|U|h|k|epsilon|omega)Final"
    {
        solver          PBiCG;
        preconditioner
        {
            preconditioner  DILU;

        }
        tolerance       1e-08;
        relTol          1e-05;
    }

}

PIMPLE
{
    momentumPredictor yes;
    nOuterCorrectors  1;
    nCorrectors       1;
    nNonOrthogonalCorrectors 0;
    rhoMin            rhoMin [ 1 -3 0 0 0 ] 0.0001;
    rhoMax            rhoMax [ 1 -3 0 0 0 ] 1000.0;

    maxCo             1.0;
    rDeltaTSmoothingCoeff 0.1;
    rDeltaTDampingCoeff 0.1;
    maxDeltaT         1;
}

relaxationFactors
{
    U               1.0;
    p               1.0;
    h               1.0;
    rho             1.0;
    T               1.0;
}

// ************************************************************************* //
