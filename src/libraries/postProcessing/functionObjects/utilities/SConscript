#Copyright (C) 2014 Applied CCM
import subprocess
Import('globalEnv')

# Clone the global environment so changes made he are not pass onto subsequent 
# libraries
funcObjUtilEnv = globalEnv.Clone()

sl = funcObjUtilEnv['SLASH']

#===============================================================================
# Additions to compiler flags for this library
#===============================================================================
funcObjUtilEnv.Append(CCFLAGS = '')

#===============================================================================
# Additions to include directories for this library
#===============================================================================
lib_inc = ['lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/sampling/lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/fvOptions/lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/lagrangian/basic/lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/lagrangian/dsmc/lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/transportModels/lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/transportModels/',
funcObjUtilEnv['LIB_SRC'] + '/turbulenceModels/',
funcObjUtilEnv['LIB_SRC'] + '/turbulenceModels/lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/turbulenceModels/incompressible/turbulenceModel/derivedFvPatchFields/wallFunctions/nutWallFunctions',
funcObjUtilEnv['LIB_SRC'] + '/turbulenceModels/compressible/turbulenceModel/derivedFvPatchFields/wallFunctions/mutWallFunctions',
funcObjUtilEnv['LIB_SRC'] + '/turbulenceModels/LES/LESdeltas/lnInclude',
funcObjUtilEnv['LIB_SRC'] + '/thermophysicalModels/basic/lnInclude'
]
funcObjUtilEnv.Prepend(CPPPATH = lib_inc)

#===============================================================================
# Additions to link library paths for this library
#===============================================================================
lib_link_path = []
funcObjUtilEnv.Prepend(LIBPATH = lib_link_path)

#===============================================================================
# Additions to link libraries for this library
#===============================================================================
lib_link = ['core','sampling','fvOptions','lagrangian',
'incompressibleTransportModels','incompressibleRASModels','incompressibleVLESModels',
'incompressibleLESModels','compressibleRASModels','compressibleVLESModels',
'compressibleLESModels','basicThermophysicalModels',
'incompressibleTurbulenceModel','compressibleTurbulenceModel'
]
funcObjUtilEnv.Append(LIBS = lib_link)
   
#===============================================================================
# Sources for this library
#===============================================================================
src_files = Split("""
  CourantNo/CourantNo.cpp
  CourantNo/CourantNoFunctionObject.cpp
  Lambda2/Lambda2.cpp
  Lambda2/Lambda2FunctionObject.cpp
  Peclet/Peclet.cpp
  Peclet/PecletFunctionObject.cpp
  Q/Q.cpp
  Q/QFunctionObject.cpp
  pressureTools/pressureTools.cpp
  pressureTools/pressureToolsFunctionObject.cpp
  residuals/residuals.cpp
  residuals/residualsFunctionObject.cpp
  scalarTransport/scalarTransport.cpp
  scalarTransport/scalarTransportFunctionObject.cpp
  timeActivatedFileUpdate/timeActivatedFileUpdate.cpp
  timeActivatedFileUpdate/timeActivatedFileUpdateFunctionObject.cpp
  turbulenceFields/turbulenceFields.cpp
  turbulenceFields/turbulenceFieldsFunctionObject.cpp
  vorticity/vorticity.cpp
  vorticity/vorticityFunctionObject.cpp
  wallShearStress/wallShearStress.cpp
  wallShearStress/wallShearStressFunctionObject.cpp
  yPlusLES/yPlusLES.cpp
  yPlusLES/yPlusLESFunctionObject.cpp
  yPlusRAS/yPlusRAS.cpp
  yPlusRAS/yPlusRASFunctionObject.cpp
  setTimeStep/setTimeStepFunctionObject.cpp
""")

#===============================================================================
# Build this library
#===============================================================================
libutilityFunctionObjects = funcObjUtilEnv.SharedLibrary(target = 
'utilityFunctionObjects', source = src_files)

#============================================================================================
# Install this library
#============================================================================================
install_dir = funcObjUtilEnv['LIB_PLATFORM_INSTALL']
funcObjUtilEnv.Alias('install', install_dir)
funcObjUtilEnv.Install(install_dir, libutilityFunctionObjects)


if (funcObjUtilEnv['WHICH_OS'] == "darwin"):

  funcObjUtilEnv.Append(SHLINKFLAGS ='-install_name @executable_path/../lib/libutilityFunctionObjects.dylib')
