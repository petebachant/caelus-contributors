/*---------------------------------------------------------------------------*\
Description
    kOmegaSST DDES LES turbulence model for incompressible flows

    References:
    1) P.R. Spalart, S. Deck, S., M.L.Shur, K.D. Squires, M.Kh Strelets, and
       A. Travin. `A new version of detached-eddy simulation, resistant to
       ambiguous grid densities'. Theor. Comp. Fluid Dyn., 20:181-195, 2006.

    2) Spalart, P. R. and Allmaras, S. R., "A One-Equation Turbulence Model for 
       Aerodynamic Flows," Recherche Aerospatiale, No. 1, 1994, pp. 5-21.

Author: A. Jemcov
Copyright Applied CCM (2014), ESI
\*---------------------------------------------------------------------------*/

#ifndef kOmegaSSTDDES_HPP
#define kOmegaSSTDDES_HPP

#include "kOmegaSSTDES.hpp"

namespace CML
{
namespace incompressible
{
namespace LESModels
{

class kOmegaSSTDDES
:
    public kOmegaSSTDES
{
    // Private Member Functions

        // Disallow default bitwise copy construct and assignment
        kOmegaSSTDDES(const kOmegaSSTDDES&);
        kOmegaSSTDDES& operator=(const kOmegaSSTDDES&);


protected:

    // Protected Member Functions

        //- Length scale
        virtual tmp<volScalarField> FDES() const;


public:

    //- Runtime type information
    TypeName("kOmegaSSTDDES");


    // Constructors

        //- Construct from components
        kOmegaSSTDDES
        (
            const volVectorField& U,
            const surfaceScalarField& phi,
            transportModel& transport,
            const word& turbulenceModelName = turbulenceModel::typeName,
            const word& modelName = typeName
        );


    //- Destructor
    virtual ~kOmegaSSTDDES()
    {}
};

} 
} 
}

#endif


