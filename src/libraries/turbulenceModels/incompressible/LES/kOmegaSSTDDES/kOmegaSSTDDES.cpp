#include "kOmegaSSTDDES.hpp"
#include "addToRunTimeSelectionTable.hpp"

namespace CML
{
namespace incompressible
{
namespace LESModels
{

defineTypeNameAndDebug(kOmegaSSTDDES, 0);
addToRunTimeSelectionTable(LESModel, kOmegaSSTDDES, dictionary);

tmp<volScalarField> kOmegaSSTDDES::FDES() const
{
    return max
           (
                Lt()/(CDES_*delta())*(scalar(1.0) - F1()),
                scalar(1.0)
           );
}


kOmegaSSTDDES::kOmegaSSTDDES
(
    const volVectorField& U,
    const surfaceScalarField& phi,
    transportModel& transport,
    const word& turbulenceModelName,
    const word& modelName
)
:
    kOmegaSSTDES(U, phi, transport, turbulenceModelName, modelName)
{}


}
}
}


